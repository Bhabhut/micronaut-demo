## MICRONAUT  DATA EXAMPLE


The primary objective of this article is to help creating an end-to-end micronaut application with JPA /hibernate support connecting to a database. And for this we will be using Micronaut Data which is similar to Spring Data in the Spring-boot world .

### Step -1 : We need a SQL Database !!!
For this part we are going to use a readymade PostgreSQL database
So once that’s done you will have a database running on your machine on
http://localhost:5432/demo with credentials postgres/postgres

### Step 2 : Scaffolding a Micronaut project
Our next step is to create micronaut project from the scratch . Micronaut now provides two options for the same. You can either create it directly from the micronaut website ( Yes ! like spring initializer) and download the code onto your system .
The second option is through Micronaut CLI . For which you need to download the micronaut cli from this location ( https://micronaut.io/download.html ) .If you are a fan of using SDKman , you have that option available. For those who like the trivial method , download the cli (Binary for Windows) from this location and then configure the path upto ‘/bin’ in your PATH variable .

Once you have successfully installed micronaut-cli you will be able to use the cli using the command “mn”
Check the “ mn help “ command and you can see the common commands available .

So for scaffolding the project you need to use the create-app command
```
mn create-app org.micronaut.demo.micronaut-hibernate-crud --build maven
```
As you can see, command will give the package structure you need for the application as a prefix to the project name. Also you can pass the build tool that you want to use as a parameter. Here in this case we have used the build tool maven. The default build tool will be gradle. i.e if you don’t give the build parameter, the source code will be generated with gradle support .

### Step-3 Configuring the Datasource
The final step before running the application is configuring the datasource to our locally running PostgreSQL database .
Before this add the runtime dependencies for the connection pool and drivers . Add the below given dependencies for HikariCP and PostgresSQL drivers .
```
<dependency>
<groupId>io.micronaut.sql</groupId>
<artifactId>micronaut-hibernate-jpa</artifactId>
<scope>compile</scope>
</dependency>
<dependency>
<groupId>org.postgresql</groupId>
<artifactId>postgresql</artifactId>
</dependency>
<dependency>
<groupId>io.micronaut.sql</groupId>
<artifactId>micronaut-jdbc-hikari</artifactId>
</dependency>
```
Configure the HikariCP connection details and JPA configurations as given below in the application.yml
```
datasources:
  default:
    url: jdbc:postgresql://localhost:5432/demo           
    driverClassName: org.postgresql.Driver
    username: postgres
    password: postgres
jpa:
 default:
   packages-to-scan:
     - ‘org.micronaut.demo’
   properties:
    hibernate:
    hbm2ddl:
      auto: update
    show_sql: true
```
### Step-4 Let’s run the application.

We are ready now .
step-1: open cmd and goto to the project directory

step-2: Let’ build the application using below command .
```
mvn clean install
```

step-3: Once it’s successfully built use the below command to run the micronaut application .
```
mvn mn:run
```
You will be seeing an output like below if everything goes well .
```
Running: http://localhost:8080
```

You can check http://localhost:8080/users to see the list of users already created in the PostgreSQL database and verify if the application is working.

Now, it's all set and you can check all the api through the postman.

### useful links

https://walkingtree.tech/micronaut-potential-poster-boy-microservices/

https://walkingtreetech.medium.com/spring-boot-vs-micronaut-the-battle-unleashed-2682354a88e9

https://micronaut.io/download.html

